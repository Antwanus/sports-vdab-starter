package com.realdolmen.sports.service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.realdolmen.sports.domain.Sportsbranch;
import com.realdolmen.sports.repo.SportsbranchDAO;

public class SportsbranchService {

private SportsbranchDAO dao;
	
	public SportsbranchService(SportsbranchDAO dao) {
		this.dao = dao;
	}
	
	public String allRegistrationsToJson() {
		List<Sportsbranch> sportbranches = dao.findAllSportsbranches();
		if (sportbranches.isEmpty()) {
			return "not found D:";
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		
		return gson.toJson(sportbranches);
	}
	
	protected FileWriter fileWriter;
	
	public void writeToFile(String json) {
		File file;
		try {
			file = new File("./", "sportsbranch.json");
			if (fileWriter == null) {
				fileWriter = new FileWriter(file);
			}
			System.out.println(json);
			fileWriter.write(json);
			System.out.println("succes");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fileWriter != null) {
				try {
					fileWriter.flush();
					fileWriter.close();
				} catch (IOException e2) {
					e2.printStackTrace();
				}
			}
		}
	}
}
