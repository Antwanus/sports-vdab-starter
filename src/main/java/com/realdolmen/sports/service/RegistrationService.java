package com.realdolmen.sports.service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.realdolmen.sports.domain.Registration;
import com.realdolmen.sports.repo.RegistrationDAO;

public class RegistrationService {
	
	private RegistrationDAO dao;
	
	public RegistrationService(RegistrationDAO dao) {
		this.dao = dao;
	}
	
	public String allRegistrationsToJson() {
		List<Registration> registrations = dao.findAllParticipants();
		if (registrations.isEmpty()) {
			return "not found D:";
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		
		return gson.toJson(registrations);
	}
	
	protected FileWriter fileWriter;
	
	public void writeToFile(String json) {
		File file;
		try {
			file = new File("./", "registration.json");
			if (fileWriter == null) {
				fileWriter = new FileWriter(file);
			}
			System.out.println(json);
			fileWriter.write(json);
			System.out.println("succes");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fileWriter != null) {
				try {
					fileWriter.flush();
					fileWriter.close();
				} catch (IOException e2) {
					e2.printStackTrace();
				}
			}
		}
	}
}
