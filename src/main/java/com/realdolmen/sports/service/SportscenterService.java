package com.realdolmen.sports.service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.realdolmen.sports.domain.Sportscenter;
import com.realdolmen.sports.repo.SportscenterDAO;

public class SportscenterService {

private SportscenterDAO dao;
	
	public SportscenterService(SportscenterDAO dao) {
		this.dao = dao;
	}
	
	public String allRegistrationsToJson() {
		List<Sportscenter> sportcenters = dao.findAllSportcenters();
		if (sportcenters.isEmpty()) {
			return "not found D:";
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		
		return gson.toJson(sportcenters);
	}
	
	protected FileWriter fileWriter;
	
	public void writeToFile(String json) {
		File file;
		try {
			file = new File("./", "sportscenters.json");
			if (fileWriter == null) {
				fileWriter = new FileWriter(file);
			}
			System.out.println(json);
			fileWriter.write(json);
			System.out.println("succes");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fileWriter != null) {
				try {
					fileWriter.flush();
					fileWriter.close();
				} catch (IOException e2) {
					e2.printStackTrace();
				}
			}
		}
	}
}
