package com.realdolmen.sports.service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.realdolmen.sports.domain.Participant;
import com.realdolmen.sports.repo.ParticipantDAO;

public class ParticipantService {

	private ParticipantDAO dao;

	public ParticipantService(ParticipantDAO dao) {
		this.dao = dao;
	}

	public String allParticipantsToJson() {
		List<Participant> participants = dao.findAllParticipants();
		if (participants.isEmpty()) {
			return "not found";
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		return gson.toJson(participants);
	}

	public String participantForIdToJson(int id) {
		Participant p = dao.findParticipantById(id);
		if (p == null) {
			return "not found";
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		return gson.toJson(p);
	}

	protected FileWriter fileWriter;

	public void writeToFile(String json) {
		File file;
		try {
			file = new File("./", "participant.json");
			if (fileWriter == null) {
				fileWriter = new FileWriter(file);
			}
			System.out.println(json);
			fileWriter.write(json);
			System.out.println("Success");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fileWriter != null) {
				try {
					fileWriter.flush();
					fileWriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		}

	}
}
