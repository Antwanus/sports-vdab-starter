package com.realdolmen.sports.service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.realdolmen.sports.domain.Camp;
import com.realdolmen.sports.repo.CampDAO;

public class CampService {

	private CampDAO dao;

	public CampService(CampDAO dao) {
		this.dao = dao;
	}

	public String allCampsToJson() {
		List<Camp> camps = dao.findAllCamps();
		if (camps.isEmpty()) {
			return "not found D:";
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		return gson.toJson(camps);
	}

	public String campForIdToJson(int id) {
		Camp c = dao.findCampById(id);
		if (c == null) {
			return "not found D:";
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		return gson.toJson(c);
	}

	protected FileWriter fileWriter;

	public void writeToFile(String json) {
		File file;
		try {
			file = new File("./", "camp.json");
			if (fileWriter == null) {
				fileWriter = new FileWriter(file);
			}
			System.out.println(json);
			fileWriter.write(json);
			System.out.println("Succes");

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fileWriter != null) {
				try {
					fileWriter.flush();
					fileWriter.close();
				} catch (IOException e2) {
					e2.printStackTrace();
				}
			}
		}
	}
}
