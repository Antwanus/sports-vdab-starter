package com.realdolmen.sports.domain;

public class CampBuilder {

	private Camp camp;

	public CampBuilder() {
		camp = new Camp();
	}
	
	public Camp build() {
		return camp;
	}

	public CampBuilder setId(int id) {
		this.camp.setId(id);
		return this;
	}

	public CampBuilder setCenter_id(int center_id) {
		this.camp.setCenter_id(center_id);
		return this;
	}

	public CampBuilder setCampname(String campname) {
		this.camp.setCampname(campname);
		return this;
	}

	public CampBuilder setStartdate(String startdate) {
		this.camp.setStartdate(startdate);
		return this;
	}

	public CampBuilder setEnddate(String enddate) {
		this.camp.setEnddate(enddate);
		return this;
	}

	public CampBuilder setSportbranch_id(int sportbranch_id) {
		this.camp.setSportbranch_id(sportbranch_id);
		return this;
	}

	public CampBuilder setMin_birthyear(int min_birthyear) {
		this.camp.setMin_birthyear(min_birthyear);
		return this;
	}

	public CampBuilder setMax_birthyear(int max_birthyear) {
		this.camp.setMax_birthyear(max_birthyear);
		return this;
	}

	public CampBuilder setAvailable_spots(double available_spots) {
		this.camp.setAvailable_spots(available_spots);
		return this;
	}
	public CampBuilder setPrice(double price) {
		this.camp.setPrice(price);
		return this;
	}
}
