package com.realdolmen.sports.domain;

public class SportscenterBuilder {

	private Sportscenter sportscenter;

	public SportscenterBuilder() {
		sportscenter = new Sportscenter();
	}

	public Sportscenter build() {
		return sportscenter;
	}

	public SportscenterBuilder setId(int id) {
		this.sportscenter.setId(id);
		return this;
	}

	public SportscenterBuilder setCentername(String centername) {
		this.sportscenter.setCentername(centername);
		return this;
	}

	public SportscenterBuilder setStreet(String street) {
		this.sportscenter.setStreet(street);
		return this;
	}

	public SportscenterBuilder setHousenumber(String housenumber) {
		this.sportscenter.setHousenumber(housenumber);
		return this;
	}

	public SportscenterBuilder setPostalcode(int postalcode) {
		this.sportscenter.setPostalcode(postalcode);
		return this;
	}

	public SportscenterBuilder setCity(String city) {
		this.sportscenter.setCity(city);
		return this;
	}

	public SportscenterBuilder setPhone(String phone) {
		this.sportscenter.setPhone(phone);
		return this;
	}

	

}
