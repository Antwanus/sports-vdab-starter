package com.realdolmen.sports.domain;

import java.time.LocalDate;

public class RegistrationBuilder {
	
	private Registration registration;
	
	public RegistrationBuilder() {
		registration = new Registration();
	}
	public Registration build() {
		return registration;
	}
	public RegistrationBuilder setParticipant_id(int participant_id) {
		this.registration.setParticipant_id(participant_id);
		return this;
	}
	public RegistrationBuilder setCamp_id(int camp_id) {
		this.registration.setCamp_id(camp_id);
		return this;
	}
	public RegistrationBuilder setRegistrationdate(String registrationdate) {
		this.registration.setRegistrationdate(registrationdate);
		return this;
	}
}