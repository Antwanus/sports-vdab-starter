package com.realdolmen.sports.domain;

public class ParticipantAddress {

	 /* street varchar(30) not null, 
	 * housenumber varchar(6) not null, 
	 * bus int,
	 * postalcode int not null, 
	 * city varchar(30), 
	 */
	private String street;
	private String houseNumber;
	private int bus;
	private int postalCode;
	private String city;
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getHouseNumber() {
		return houseNumber;
	}
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}
	public int getBus() {
		return bus;
	}
	public void setBus(int bus) {
		this.bus = bus;
	}
	public int getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(int postalCode) {
		this.postalCode = postalCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	
}
