package com.realdolmen.sports.domain;

public class SportsbranchBuilder {

	private Sportsbranch sportsbranch;

	public SportsbranchBuilder() {
		sportsbranch = new Sportsbranch();
	}
	public Sportsbranch build() {
		return sportsbranch;
	}
	public SportsbranchBuilder setId(int id) {
		this.sportsbranch.setId(id);
		return this;
	}
	public SportsbranchBuilder setOmschrijving(String omschrijving) {
		this.sportsbranch.setOmschrijving(omschrijving);
		return this;
	}
}