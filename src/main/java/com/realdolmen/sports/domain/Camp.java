package com.realdolmen.sports.domain;

public class Camp {
	private int id;
	private int center_id;
	private String campname;
	private String startdate;
	private String enddate;
	private int sportbranch_id;
	private int min_birthyear;
	private int max_birthyear;
	private double price;
	private double available_spots;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCenter_id() {
		return center_id;
	}
	public void setCenter_id(int center_id) {
		this.center_id = center_id;
	}
	public String getCampname() {
		return campname;
	}
	public void setCampname(String campname) {
		this.campname = campname;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public int getSportbranch_id() {
		return sportbranch_id;
	}
	public void setSportbranch_id(int sportbranch_id) {
		this.sportbranch_id = sportbranch_id;
	}
	public int getMin_birthyear() {
		return min_birthyear;
	}
	public void setMin_birthyear(int min_birthyear) {
		this.min_birthyear = min_birthyear;
	}
	public int getMax_birthyear() {
		return max_birthyear;
	}
	public void setMax_birthyear(int max_birthyear) {
		this.max_birthyear = max_birthyear;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getAvailable_spots() {
		return available_spots;
	}
	public void setAvailable_spots(double available_spots) {
		this.available_spots = available_spots;
	}
	
	

}
