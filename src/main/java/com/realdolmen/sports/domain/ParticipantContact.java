package com.realdolmen.sports.domain;

public class ParticipantContact {
	 /* phone varchar(10), 
	 * mobile varchar(15), 
	 * email varchar(40)
	 */
	private String phone;
	private String mobile;
	private String email;
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
