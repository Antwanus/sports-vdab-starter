package com.realdolmen.sports;

import com.realdolmen.sports.repo.CampDAO;
import com.realdolmen.sports.repo.ParticipantDAO;
import com.realdolmen.sports.repo.RegistrationDAO;
import com.realdolmen.sports.repo.SportsbranchDAO;
import com.realdolmen.sports.repo.SportscenterDAO;
import com.realdolmen.sports.service.CampService;
import com.realdolmen.sports.service.ParticipantService;
import com.realdolmen.sports.service.RegistrationService;
import com.realdolmen.sports.service.SportsbranchService;
import com.realdolmen.sports.service.SportscenterService;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) {
    	
       ParticipantService partiService = new ParticipantService(new ParticipantDAO());
       String partJson = partiService.allParticipantsToJson();
       partiService.writeToFile(partJson);
       
       CampService campService = new CampService(new CampDAO());
       String campJson = campService.allCampsToJson();
       campService.writeToFile(campJson);
       
       RegistrationService regiService = new RegistrationService(new RegistrationDAO());
       String regiJson = regiService.allRegistrationsToJson();
       regiService.writeToFile(regiJson);
       
       SportsbranchService branchService = new SportsbranchService(new SportsbranchDAO());
       String branchJson = branchService.allRegistrationsToJson();
       branchService.writeToFile(branchJson);
       
       SportscenterService centerService = new SportscenterService(new SportscenterDAO());
       String centerJson = centerService.allRegistrationsToJson();
       centerService.writeToFile(centerJson);
       
       ParticipantDAO.addParticipant("De Eerste", "Antoon", "1997-01-01", "Y", "straat", 
    		   "1", 0, 1990, "Antwerpen", "0000000000", "0000/00 00 00", "a@email.com");
       
       
    }
}
