package com.realdolmen.sports.repo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.realdolmen.sports.domain.Registration;
import com.realdolmen.sports.domain.RegistrationBuilder;

public class RegistrationDAO {

	private static Connection connection;
	private static final String USER_NAME = "root";
	private static final String PASS_WORD = "";
	private static final String URL = "jdbc:mysql://localhost:3306/sports";

	public static Connection initConnection(String user, String pass, String jdbcUrl) throws SQLException {
		connection = DriverManager.getConnection(jdbcUrl, user, pass);
		return connection;
	}

	protected List<Registration> executer(String query) {
		return this.execute(query);
	}

	private List<Registration> execute(String query) {
		List<Registration> registrations = new ArrayList<Registration>();
		try {
			if (connection == null || connection.isClosed()) {
				initConnection(USER_NAME, PASS_WORD, URL);
			}
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(query);
			while (rs.next()) {
				registrations.add(new RegistrationBuilder()
						.setParticipant_id(rs.getInt("participant_id"))
						.setCamp_id(rs.getInt("camp_id"))
						.setRegistrationdate(rs.getString("regisrationdate"))
						.build()
						);	
			}
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e2) {
					e2.printStackTrace();
				}
			}
		}
		return registrations;
	}
	
	public static void addRegistration(int participant_id, int camp_id, String registration_date) {
		try {
			if (connection == null || connection.isClosed()) {
				initConnection(USER_NAME, PASS_WORD, URL);
			}
			PreparedStatement ps = connection.prepareStatement("INSERT INTO registration (participant_id, "
					+ "camp_id, registration_date) VALUES (?, ?, ?)");
			ps.setInt(1, participant_id);
			ps.setInt(2, camp_id);
			ps.setString(3, registration_date);
			ps.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<Registration> findAllParticipants(){
		return execute("SELECT * FROM registration");
	}
	
	public Registration findRegistrationById(int id) {
		List<Registration> result = execute("SELECT * FROM registration WHERE id = " + id);
		return result.isEmpty()	? null : result.get(0);
	}

}
