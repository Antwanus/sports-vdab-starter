package com.realdolmen.sports.repo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.realdolmen.sports.domain.Camp;
import com.realdolmen.sports.domain.CampBuilder;

public class CampDAO {
	
	private static Connection connection;
	private static final String USER_NAME = "root";
	private static final String PASS_WORD = "";
	private static final String URL = "jdbc:mysql://localhost:3306/sports";

	public static Connection initConnection(String user, String pass, String jdbcUrl) throws SQLException {
		connection = DriverManager.getConnection(jdbcUrl, user, pass);
		return connection;
	}
	
	protected List<Camp> executer(String query){
		return this.execute(query);
	}

	private List<Camp> execute(String query) {
		List<Camp> camps = new ArrayList<Camp>();
		try {
			if (connection == null || connection.isClosed()) {
				initConnection(USER_NAME, PASS_WORD, URL);
			}
			Statement statement = connection.createStatement();
			ResultSet result = statement.executeQuery(query);
			while (result.next()) {
				camps.add(new CampBuilder()
						.setId(result.getInt("id"))
						.setCenter_id(result.getInt("center_id"))
						.setCampname(result.getString("campname"))
						.setStartdate(result.getString("startdate"))
						.setEnddate(result.getString("enddate"))
						.setMin_birthyear(result.getInt("min_birthyear"))
						.setMax_birthyear(result.getInt("max_birthyear"))
						.setPrice(result.getDouble("price"))
						.setAvailable_spots(result.getDouble("available_spots"))
						.build()
						);
			}
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e2) {
					e2.printStackTrace();
				}
			}
		}
		return camps;
	}
	
	public static void addCamp(int camp_id, String campname, String startdate, String enddate,
			int min_birthyear, int max_birthyear, double price, double available_spots) {
		try {
			if (connection == null || connection.isClosed()) {
				initConnection(USER_NAME, PASS_WORD, URL);
			}
			PreparedStatement ps = connection.prepareStatement("INSERT INTO camp (camp_id,"
					+ "campname, startdate, enddate, min_birthyear, max_birthyear, price, available_spots)"
					+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
			ps.setInt(1, camp_id);
			ps.setString(2, campname);
			ps.setString(3, startdate);
			ps.setString(4, enddate);
			ps.setInt(5, min_birthyear);
			ps.setInt(6, max_birthyear);
			ps.setDouble(7, price);
			ps.setDouble(8, available_spots);
			ps.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<Camp> findAllCamps(){
		return execute("SELECT * FROM camp");
	}
	
	public Camp findCampById(int id) {
		List<Camp> result = execute("SELECT * FROM camp WHERE id=" + id);
		return result.isEmpty() ? null : result.get(0);
		
	}

}
