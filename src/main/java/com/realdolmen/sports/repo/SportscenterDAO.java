package com.realdolmen.sports.repo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.realdolmen.sports.domain.Sportscenter;
import com.realdolmen.sports.domain.SportscenterBuilder;

public class SportscenterDAO {

	private static Connection connection;
	private static final String USER_NAME = "root";
	private static final String PASS_WORD = "";
	private static final String URL = "jdbc:mysql://localhost:3306/sports";

	public static Connection initConnection(String user, String pw, String url) throws SQLException {
		connection = DriverManager.getConnection(url, user, pw);
		return connection;
	}

	protected List<Sportscenter> executer(String query) {
		return this.execute(query);
	}

	private List<Sportscenter> execute(String query) {
		List<Sportscenter> sportscenters = new ArrayList<Sportscenter>();
		try {
			if (connection == null || connection.isClosed()) {
				initConnection(USER_NAME, PASS_WORD, URL);
			}
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(query);
			while (rs.next()) {
				sportscenters.add(new SportscenterBuilder()
						.setCentername(rs.getString("centername"))
						.setStreet(rs.getString("street"))
						.setHousenumber(rs.getString("housenumber"))
						.setPostalcode(rs.getInt("postalcode"))
						.setCity(rs.getString("city"))
						.setPhone(rs.getString("phone"))
						.build()
						);
			}
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e2) {
					e2.printStackTrace();
				}
			}
		}
		return sportscenters;
	}

	public static void addSportscenter(String centername, String street, String housenumber, 
			int postalcode, String city, String phone) {
		try {
			if (connection == null || connection.isClosed()) {
				initConnection(USER_NAME, PASS_WORD, URL);
			}
			PreparedStatement ps = connection.prepareStatement("INSERT INTO sportscenter (centername, "
					+ "street, housenumber, postalcode, city, phone) VALUES (?, ?, ?, ?, ?, ?)");
			ps.setString(1, centername);
			ps.setString(2, street);
			ps.setString(3, housenumber);
			ps.setInt(4, postalcode);
			ps.setString(5, city);
			ps.setString(6, phone);
			ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public List <Sportscenter> findAllSportcenters(){
		return execute("SELECT * FROM sportscenter");
	}
	public Sportscenter findSportscenterById(int id) {
		List<Sportscenter> result = execute("SELECT * FROM sportscenter WHERE id= " + id);
		return result.isEmpty()	? null : result.get(0);
	}
}
