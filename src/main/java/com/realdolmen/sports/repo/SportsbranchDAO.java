package com.realdolmen.sports.repo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.realdolmen.sports.domain.Sportsbranch;
import com.realdolmen.sports.domain.SportsbranchBuilder;

public class SportsbranchDAO {

	private static Connection connection;
	private static final String USER_NAME = "root";
	private static final String PASS_WORD = "";
	private static final String URL = "jdbc:mysql://localhost:3306/sports";
	
	public static Connection initConnection(String user, String pw, String url) throws SQLException {
		connection = DriverManager.getConnection(url, user, pw);
		return connection;
	}
	
	protected List<Sportsbranch> executer(String query){
		return this.execute(query);
	}

	private List<Sportsbranch> execute(String query) {
		List<Sportsbranch> sportsbranches = new ArrayList<Sportsbranch>();
		try {
			if (connection == null || connection.isClosed()) {
				initConnection(USER_NAME, PASS_WORD, URL);
			}
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(query);
			while (rs.next()) {
				sportsbranches.add(new SportsbranchBuilder()
						.setOmschrijving(rs.getString("omschrijving"))
						.build()
						);
			}
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e2) {
					e2.printStackTrace();
				}
			}
		}
		return sportsbranches;
	}
	
	public static void addSportsbranch(String omschrijving) {
		try {
			if (connection == null || connection.isClosed()) {
				initConnection(USER_NAME, PASS_WORD, URL);
			}
			PreparedStatement ps = connection.prepareStatement("INSERT INTO sportsbranch "
					+ "(omschrijving) VALUES (?)");
			ps.setString(1, omschrijving);
			ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public List <Sportsbranch> findAllSportsbranches(){
		return execute("SELECT * FROM sportsbranch");
	}
	
	public Sportsbranch findSportsbranchById(int id) {
		List <Sportsbranch> result = execute("SELECT * FROM registration WHERE id = " + id);
		return result.isEmpty() ? null : result.get(0);
	}
}
