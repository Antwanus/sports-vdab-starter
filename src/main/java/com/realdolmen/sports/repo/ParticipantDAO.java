package com.realdolmen.sports.repo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.realdolmen.sports.domain.Participant;
import com.realdolmen.sports.domain.ParticipantBuilder;

public class ParticipantDAO {

	private static Connection connection;
	private static final String USER_NAME = "root";
	private static final String PASS_WORD = "";
	private static final String URL = "jdbc:mysql://localhost:3306/sports";

	public static Connection initConnection(String user, String pass, String jdbcUrl) throws SQLException {
		connection = DriverManager.getConnection(jdbcUrl, user, pass);
		return connection;
	}

	protected List<Participant> executer(String query) {
		return this.execute(query);
	}

	private List<Participant> execute(String query) {
		List<Participant> participants = new ArrayList<Participant>();
		try {
			if (connection == null || connection.isClosed()) {
				initConnection(USER_NAME, PASS_WORD, URL);
			}
			Statement statement = connection.createStatement();
			ResultSet result = statement.executeQuery(query);
			while (result.next()) {
				participants.add(
						new ParticipantBuilder()
						.setId(result.getInt("id"))
						.setFirstName(result.getString("firstname"))
						.setName(result.getString("participantname"))
						.build());

			}
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return participants;
	}

	public static void addParticipant(String participantname, String firstname, String birthdate, String sex,
			String street, String housenumber, int bus, int postalcode, String city, String phone,
			String mobile, String email) {
		try (
				Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sports?useSSL=false", "root",	"");
			) {
			java.sql.PreparedStatement ps = con.prepareStatement
					("INSERT INTO participant (participantname, firstname, birthdate, sex, street,"
							+ " housenumber, bus, postalcode, city, phone, mobile, email) "
							+ "VALUES	(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

			ps.setString(1, participantname);
			ps.setString(2, firstname);
			ps.setString(3, birthdate);
			ps.setString(4, sex);
			ps.setString(5, street);
			ps.setString(6, housenumber);
			ps.setInt(7, bus);
			ps.setInt(8, postalcode);
			ps.setString(9, city);
			ps.setString(10, phone);
			ps.setString(11, mobile);
			ps.setString(12, email);
			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<Participant> findAllParticipants() {
		return execute("SELECT * from participant");
	}

	public Participant findParticipantById(int id) {
		List<Participant> result = execute("SELECT * from participant where id=" + id);
		return result.isEmpty() ? null : result.get(0);
	}

}
